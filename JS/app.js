// Теоретичні питання
// 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
// Для створення вузлів DOM існують два методи:document.createElement(), document.createTextNode(text)
// Щоб зробити div видимимим, нам потрібно вставити його куди-небудь в документ: append

// 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
// let element = document.querySelector('.navigation');
// if (element) {
    // element.remove();
// }
// Нам потрібно спочатку знайти наш клас, а потім за допомогою команди remove видалити його

// 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
    // node.append(...nodes або strings) – додає вузли або рядки в кінець вузла,
    // node.prepend(...nodes або strings) – вставляє вузли або рядки в початок вузла,
    // node.before(...nodes або strings) – вставляє вузли або рядки перед вузлом,
    // node.after(...nodes або strings) – вставляє вузли або рядки після вузла,
    // node.replaceWith(...nodes або strings) – замінює вузол заданими вузлами або рядками.


// Практичні завдання
// 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#".
// Додайте цей елемент в footer після параграфу.

let newElement = document.createElement("a");
newElement.textContent = 'Learn More';
newElement.href = '#';
let footer = document.createElement('footer')
let paragraph = document.createElement('p')
document.body.append(footer)
footer.append(paragraph)
footer.append(newElement)

// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
// Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

const select = document.createElement('select');
select.id = 'raiting';
const main = document.createElement('main');
const section = document.createElement('section');
section.classList.add('Features');

const options = [
    { value: '4', text: '4 Stars' },
    { value: '3', text: '3 Stars' },
    { value: '2', text: '2 Stars' },
    { value: '1', text: '1 Star' }
];

options.forEach(optionData => {
    const option = document.createElement('option');
    option.value = optionData.value;
    option.text = optionData.text;
    select.appendChild(option);
})

document.body.append(main);
main.append(section);
section.before(select);
